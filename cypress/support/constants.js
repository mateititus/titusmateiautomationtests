export const airbnbUrl = "airbnb.com";
export const afterSearchHeaderButtons = ["Rome", "Nov 10 – 17", "3 guests"];
export const filterPropertiesUrl =
  "https://www.airbnb.com/s/Rome--Italy/homes?tab_id=home_tab&refinement_paths%5B%5D=%2Fhomes&flexible_trip_lengths%5B%5D=one_week&price_filter_input_type=0&price_filter_num_nights=7&min_bedrooms=5&search_type=user_map_move&amenities%5B%5D=7&query=Rome%2C%20Italy&place_id=ChIJw0rXGxGKJRMRAIE4sppPCQM&date_picker_type=calendar&checkin=2022-11-10&checkout=2022-11-17&adults=2&children=1&source=structured_search_input_header&ne_lat=42.376922884213634&ne_lng=13.51500409165962&sw_lat=41.76118536384692&sw_lng=11.64458172837837&zoom=9&search_by_map=true";

// Selectors

export const anywhereButtonSelector = '[data-index="0"]';
export const searchDestinationsInputSelector =
  '[data-testid="structured-search-input-field-query"]';
export const searchResultsDropDownOptionsSelector = '[class="_r1t6ga"]';
export const checkInButtonSelector =
  '[data-testid="structured-search-input-field-split-dates-0"]';
export const checkOutButtonSelector =
  '[data-testid="structured-search-input-field-split-dates-1"]';
export const addGuestsButtonSelector =
  '[data-testid="structured-search-input-field-guests-button"]';
export const calendarDaySelector = '[data-testid*="calendar-day"]';
export const guestsPanelSelector =
  '[data-testid="structured-search-input-field-guests-panel"]';
export const adultsIncreaseButtonSelector =
  '[data-testid="stepper-adults-increase-button"]';
calendarDaySelector;
export const childrenIncreaseButtonSelector =
  'data-testid="stepper-children-increase-button"';
calendarDaySelector;
export const adultsGuestsValue = '[data-testid="stepper-adults-value"]';
export const childrenGuestsValue = '[data-testid="stepper-children-value"]';
export const searchInputSearchButtonSelector =
  '[data-testid="structured-search-input-search-button"]';
export const afterSearchHeaderButtonsSelector = '[class*="f1xx50dm"]';
export const filtersButtonSelector = '[class*="v1tureqs"]';
export const fiveQuantityNumberSelector = '[data-testid="menuItemButton-5"]';
export const filterShowMoreButtonSelector = '[class*="lnq7699"]';
export const filtersPoolButtonSelector =
  '[id$="checkbox-amenities-7-row-title"]';
export const showHomesButtonSelector = '[class*="1ku51f04"]';
export const propertiesPictureSelector = '[loading="eager"]';
export const propertyDetailsSelector = '[id="title_3252151"]';
