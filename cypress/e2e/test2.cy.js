import * as constants from "../support/constants.js";

const airbnbUrl = constants.airbnbUrl;

const filtersButtonSelector = constants.filtersButtonSelector;
const fiveQuantityNumberSelector = constants.fiveQuantityNumberSelector;
const filterShowMoreButtonSelector = constants.filterShowMoreButtonSelector;
const filtersPoolButtonSelector = constants.filtersPoolButtonSelector;
const showHomesButtonSelector = constants.showHomesButtonSelector;
const propertiesPictureSelector = constants.propertiesPictureSelector;

describe("Verify that the results and details page match the extra filters", () => {
  it("Search for properties that match the same filters as the first test.", () => {
    cy.visit("/", {
      headers: {
        accept: "application/json, text/plain, */*",
        "user-agent": "axios/0.27.2",
      },
    });
    cy.url().should("include", airbnbUrl);
  });
  it("Click More filters.", () => {
    cy.get(filtersButtonSelector).should("be.visible").click();
  });
  it("Select the number of bedrooms as 5 and Pool from the Facilities section.", () => {
    cy.get(fiveQuantityNumberSelector).eq(0).scrollIntoView().click();
    cy.get(filterShowMoreButtonSelector).eq(0).scrollIntoView().click();
    cy.get(filtersPoolButtonSelector).scrollIntoView().click();
  });
  it("Click Show Stays and verify that the properties displayed on the first page have at least the number of selected bedrooms.", () => {
    cy.get(showHomesButtonSelector).click();
    cy.wait(5000);
    cy.get(propertiesPictureSelector).eq(0).click();
  });
});
