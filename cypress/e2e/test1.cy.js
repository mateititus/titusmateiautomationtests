import * as constants from "../support/constants.js";

const airbnbUrl = constants.airbnbUrl;
const afterSearchHeaderButtons = constants.afterSearchHeaderButtons;

const anywhereButtonSelector = constants.anywhereButtonSelector;
const searchDestinationsInputSelector =
  constants.searchDestinationsInputSelector;
const searchResultsDropDownOptionsSelector =
  constants.searchResultsDropDownOptionsSelector;
const calendarDaySelector = constants.calendarDaySelector;
const checkInButtonSelector = constants.checkInButtonSelector;
const checkOutButtonSelector = constants.checkOutButtonSelector;
const addGuestsButtonSelector = constants.addGuestsButtonSelector;
const adultsIncreaseButtonSelector = constants.adultsIncreaseButtonSelector;
const adultsGuestsValue = constants.adultsGuestsValue;
const childrenIncreaseButtonSelector = constants.childrenIncreaseButtonSelector;
const childrenGuestsValue = constants.childrenGuestsValue;
const searchInputSearchButtonSelector =
  constants.searchInputSearchButtonSelector;
const afterSearchHeaderButtonsSelector =
  constants.afterSearchHeaderButtonsSelector;
const guestsPanelSelector = constants.guestsPanelSelector;

describe("Verify that the results match the search criteria", () => {
  it("Open www.airbnb.com.", () => {
    cy.visit("/", {
      headers: {
        accept: "application/json, text/plain, */*",
        "user-agent": "axios/0.27.2",
      },
    });
    cy.url().should("include", airbnbUrl);
  });
  it("Select Rome, Italy as a location.", () => {
    cy.get(anywhereButtonSelector).should("be.visible").click();
    cy.get(searchDestinationsInputSelector)
      .should("be.visible")
      .type("Rome, Italy");
    cy.get(searchResultsDropDownOptionsSelector)
      .should("be.visible")
      .and("have.text", "Rome, Italy");
  });
  it("Pick a Check-In date one week after the current date.", () => {
    cy.get(checkInButtonSelector)
      .should("be.visible")
      .and("have.attr", "aria-expanded", "true");
    cy.get(calendarDaySelector)
      .should("be.visible")
      .and("have.text", "10")
      .and("have.attr", "data-is-day-blocked", "false")
      .click();
    cy.get(checkInButtonSelector)
      .should("be.visible")
      .and("have.attr", "aria-expanded", "false");
  });
  it("Pick a Check-Out date one week after the Check-In date.", () => {
    cy.get(checkOutButtonSelector)
      .should("be.visible")
      .and("have.attr", "aria-expanded", "true");
    cy.get(calendarDaySelector)
      .should("be.visible")
      .and("have.text", "17")
      .and("have.attr", "data-is-day-blocked", "false")
      .click();
    cy.get(checkOutButtonSelector)
      .should("be.visible")
      .and("have.attr", "aria-expanded", "false");
  });
  it("Select the number of guests as 2 adults and 1 child.", () => {
    cy.get(addGuestsButtonSelector).should("be.visible").click();
    cy.get(guestsPanelSelector).should("be.visible");
    cy.get(adultsIncreaseButtonSelector).should("be.visible").dblclick();
    cy.get(adultsGuestsValue).should("have.value", "2");
    cy.get(childrenIncreaseButtonSelector).should("be.visible").click();
    cy.get(childrenGuestsValue).should("have.value", "1");
  });
  it("Search for properties and verify that the applied filters are correct.", () => {
    cy.get(searchInputSearchButtonSelector).should("be.visible").click();
    for (let i = 0; i < afterSearchHeaderButtons.length; i++) {
      cy.get(afterSearchHeaderButtonsSelector)
        .should("be.visible")
        .and("have.text", afterSearchHeaderButtons[i]);
    }
  });
});
