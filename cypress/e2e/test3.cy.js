import * as constants from "../support/constants.js";

const filterPropertiesUrl = constants.filterPropertiesUrl;

const propertiesPictureSelector = constants.propertiesPictureSelector;
const propertyDetailsSelector = constants.propertyDetailsSelector;

describe("Verify that a property is displayed on the map correctly", () => {
  it("Search for properties that match the same filters as the first test.", () => {
    cy.visit(filterPropertiesUrl, {
      headers: {
        accept: "application/json, text/plain, */*",
        "user-agent": "axios/0.27.2",
      },
    });
  });
  it("Hover over the first property in the results list and that the property is displayed on the map.", () => {
    cy.get(propertiesPictureSelector).eq(0).trigger("mouseover");
    cy.wait(5000);
    cy.contains("$308")
      .eq(1)
      .should("have.css", "background-color", "var(--f-k-smk-x)");
  });
  it("Click on the property and verify details are the same", () => {
    cy.contains("$308").eq(1).should("be.visible").click();
    for (let i = 0; i < 2; i++) {
      cy.get(propertyDetailsSelector)
        .eq(i)
        .should("be.visible")
        .and("have.text", "Villa in Sacrofano");
    }
  });
});
